+function ($) {
	$(document).ready(function(){
		$('.carousel-slide').slick({
			dots: true,
			arrows: false,
			responsive: [
				{
					breakpoint: 979,
					settings: {
						dots: false
					}
				}
			]
		});

      	$('.feature-slide').slick({
      		slidesToShow: 3,
			slidesToScroll: 1,
			centerPadding: '67px',
			dots: true,
			centerMode: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						dots: false,
						centerPadding: '0',
					}
				}
			]
      	});
    });

    if ($(window).width() < 768) {
		if (! $(".section-grid-s1 .row").hasClass("slick-slider") ) {
			$('.section-grid-s1 .row').slick({
				dots: false,
			});
		};

		if (! $(".section-grid-s1 .row").hasClass("slick-slider") ) {
			$('.section-grid-s1 .row').slick({
				dots: false,
			});
		};		
	}

	$( window ).resize(function() {
		if ($(window).width() < 768) {
			if (! $(".section-grid-s1 .row").hasClass("slick-slider") ) {
				$('.section-grid-s1 .row').slick({
					dots: false,
				});
			};
		} else {
			$('.section-grid-s1 .row').slick('unslick');
		}
	});
}(jQuery);